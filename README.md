# NotificationHub SDK (php)

This is a small php sdk for our notification hub

## Get Started

You will need to initialize a useable api object (REST or GRPC).

```php
use NotificationHub\NotificationHubGrpcApi;
use NotificationHub\NotificationHubRestApi;
use NotificationHub\NotificationHubClient;

// initialize api
$api = new NotificationHubRestApi(Endpoint::DEVELOPMENT()->getValue());
//OR
$api = new NotificationHubGrpcApi(Endpoint::DEVELOPMENT()->getValue(), "80");

// initialize client
$hub = new NotificationHubClient($api);
```

## Endpoint

Currently there are only two different endpoints supported - dev and prod.

```php
use NotificationHub\Endpoint;

// for development
$endpoint = EventPriority::DEVELOPMENT();
// for production
$endpoint = EventPriority::PRODUCTION();
```

## Device Type

Currently there are three different supported device types - iOS, Android and Web. See below how to use them.

```php
use NotificationHub\DeviceType;

// for iOS
$deviceType = DeviceType::iOS();
// for android
$deviceType = DeviceType::ANDROID();
// for web
$deviceType = DeviceType::WEB();
```

## Event Priority

You can tell the notification hub how important the push is: high will try to push immediately and normal will consider the device time.

```php
use NotificationHub\EventPriority;

// for normal
$priority = EventPriority::NORMAL();
// for high
$priority = EventPriority::HIGH();
```

## Event

The SDK will define events, which you must use to push notifications.

```php
use NotificationHub\Event;

// Event for syncing checkaccount transactions
$event = EventPriority::CHECKACCOUNT_TRANSACTION_SYNC();
```

### Current available events

| Events        |
| ------------- |
| MARKETING     |
| CHECKACCOUNT_TRANSACTION_SYNC      | 

## Subscribe

Subscribe user device to notification hub

```php
$sevUserID = 1;
$uuid = "3334asdfx2345";
$deviceType = DeviceType::iOS();
// OR
$deviceType = DeviceType::Android();
// OR
$deviceType = DeviceType::Web();
// IMPORTANT!!! Always use UTC  !!!IMPORTANT
$startTime =  gmdate("Y-m-d\TH:i:s\Z", strtotime("2020-01-28T10:00:00+00:00")); // Optional
$endTime =  gmdate("Y-m-d\TH:i:s\Z", strtotime("2020-01-28T16:00:00+00:00")); // Optional
$result = $hub->subscribe($sevUserID, $uuid, $deviceType, $startTime, $endTime);
```

### Response

```json
{
    "id": 2,
    "sevUser": 1,
    "consumerType": 0,
    "uuid": "3157a001a99a199dd2d1b93ee2241c2241554cad2bee394b1a25fd846c026c7f"
}
```

## Unsubscribe

Unsubscribe user device from notification hub

```php
$deviceID = 1;
$result = $hub->unsubscribe($deviceID);
```

### Response

```json
{
    "id": 2,
    "sevUser": 1,
    "consumerType": 0,
    "uuid": "3157a001a99a199dd2d1b93ee2241c2241554cad2bee394b1a25fd846c026c7f"
}
```

## Devices

Subscribe user device to notification hub

```php
$sevUserID = 1;
$uuid = "3334asdfx2345";
$deviceType = DeviceType::iOS();
// OR
$deviceType = DeviceType::Android();
// OR
$deviceType = DeviceType::Web();
$result = $hub->subscribe($sevUserID, $uuid, $deviceType);
```

### Response

```json
[
    {
        "id": 2,
        "sevUser": 1,
        "consumerType": 0,
        "uuid": "3157a001a99a199dd2d1b93ee2241c2241554cad2bee394b1a25fd846c026c7f"
    }
]
```

## Push

Push message to user device

```php
$sevUserID = 1;
$event = Event::CHECKACCOUNT_TRANSACTION_SYNC();
$deviceType = DeviceType::iOS();
// OR
$deviceType = DeviceType::Android();
// OR
$deviceType = DeviceType::Web();
$priority = EventPriority::High(); // Optional
$titleKeyArgs = ["Max Mustermann"]; // Optional
$bodyKeyArgs = ["RE-2345"]; // Optional

$result = $hub->subscribe($sevUserID, $event, $deviceType, $priority, $titleKeyArgs, $bodyKeyArgs);
```

### Response

```json
{
    "success": true
}
```

## Error Codes
```php
private const BAD_REQUEST = 1; // 400 HTTP Error Code
private const SERVER_ERROR = 2; // Something went wrong and application couldnt handle it
private const CONNECTION_ERROR = 3; // No connection found or did loose connection
private const UNKNOWN_ERROR = 4; // Something went really wrong
private const NO_STRINGS = 5; // There was no translation strings for event id
private const GET_DEVICES_FAILED = 6; // Fetching devices failed
private const SUBSCRIBE_FAILED = 7; // Subscribing dedvice failed
private const UNSUBSCRIBE_FAILED = 8; // unsubscribing device failed
private const PUSH_FAILED = 9; // Creating notification failed
```