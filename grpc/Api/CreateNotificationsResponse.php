<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: grpc/notificationhub.proto

namespace Api;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;
use Google\Protobuf\Internal\GPBWrapperUtils;

/**
 * Generated from protobuf message <code>api.CreateNotificationsResponse</code>
 */
class CreateNotificationsResponse extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>repeated .api.Notification notifications = 1;</code>
     */
    private $notifications;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Api\Notification[]|\Google\Protobuf\Internal\RepeatedField $notifications
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Grpc\Notificationhub::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>repeated .api.Notification notifications = 1;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * Generated from protobuf field <code>repeated .api.Notification notifications = 1;</code>
     * @param \Api\Notification[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setNotifications($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Api\Notification::class);
        $this->notifications = $arr;

        return $this;
    }

}

