<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: grpc/notificationhub.proto

namespace Api;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;
use Google\Protobuf\Internal\GPBWrapperUtils;

/**
 * Generated from protobuf message <code>api.UnsubscribeRequest</code>
 */
class UnsubscribeRequest extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int32 deviceID = 1;</code>
     */
    private $deviceID = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int $deviceID
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Grpc\Notificationhub::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int32 deviceID = 1;</code>
     * @return int
     */
    public function getDeviceID()
    {
        return $this->deviceID;
    }

    /**
     * Generated from protobuf field <code>int32 deviceID = 1;</code>
     * @param int $var
     * @return $this
     */
    public function setDeviceID($var)
    {
        GPBUtil::checkInt32($var);
        $this->deviceID = $var;

        return $this;
    }

}

