<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Api;

/**
 */
class NotificationHubClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Api\ListDevicesRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListDevices(\Api\ListDevicesRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/api.NotificationHub/ListDevices',
        $argument,
        ['\Api\ListDevicesResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Api\SubscribeRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function Subscribe(\Api\SubscribeRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/api.NotificationHub/Subscribe',
        $argument,
        ['\Api\SubscribeResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Api\UnsubscribeRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function Unsubscribe(\Api\UnsubscribeRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/api.NotificationHub/Unsubscribe',
        $argument,
        ['\Api\UnsubscribeResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Api\CreateNotificationsRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CreateNotifications(\Api\CreateNotificationsRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/api.NotificationHub/CreateNotifications',
        $argument,
        ['\Api\CreateNotificationsResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Api\PushNotificationRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function PushNotification(\Api\PushNotificationRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/api.NotificationHub/PushNotification',
        $argument,
        ['\Api\PushNotificationResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Api\ListNotificationsRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ListNotifications(\Api\ListNotificationsRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/api.NotificationHub/ListNotifications',
        $argument,
        ['\Api\ListNotificationsResponse', 'decode'],
        $metadata, $options);
    }

}
