<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: grpc/notificationhub.proto

namespace Api;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;
use Google\Protobuf\Internal\GPBWrapperUtils;

/**
 * Generated from protobuf message <code>api.ListDevicesResponse</code>
 */
class ListDevicesResponse extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>repeated .api.Device devices = 1;</code>
     */
    private $devices;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Api\Device[]|\Google\Protobuf\Internal\RepeatedField $devices
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Grpc\Notificationhub::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>repeated .api.Device devices = 1;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getDevices()
    {
        return $this->devices;
    }

    /**
     * Generated from protobuf field <code>repeated .api.Device devices = 1;</code>
     * @param \Api\Device[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setDevices($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Api\Device::class);
        $this->devices = $arr;

        return $this;
    }

}

