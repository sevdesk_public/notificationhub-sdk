<?php
declare(strict_types=1);
namespace NotificationHub;

/**
 * PushDTO
 * 
 * 
 * @package NotificationHub
 * @subpackage Resource
 * @author sevDesk
 */
final class PushDTO {

    /**
     * @var int Identifier of current sev user
     */
    public $sevUserID;

    /**
     * @var int Identifier of current sev user
     */
    public $eventID;

    /**
     * @var NotificationData
     */
    public $data;

    /**
     * @var int
     */
    public $deviceType;

    /**
     * @var int
     */
    public $eventPriority;


    /**
     * PushDTO constructor.
     * @param int $sevUserID send notification for this user (optional)
     * @param NotificationDTO $data contains all translation strings for notification
     * @param int $deviceType who will consume the notification
     */
    public function __construct(int $sevUserID = null, int $eventID, NotificationDTO $data, int $deviceType = null, int $eventPriority){
        $this->sevUserID = $sevUserID;
        $this->eventID = $eventID;
        $this->data = $data;
        $this->deviceType = $deviceType;
        $this->eventPriority = $eventPriority;
    }
}