<?php
declare(strict_types=1);
namespace NotificationHub;
use MyCLabs\Enum\Enum;

/**
 * EventPriority
 * 
 * 
 * @package NotificationHub
 * @subpackage Resource
 * @author sevDesk
 * @method static \NotificationHub\EventPriority NORMAL()
 * @method static \NotificationHub\EventPriority HIGH()
 */
class EventPriority extends Enum
{
    const NORMAL = 0;
    const HIGH = 1;
}