<?php
declare(strict_types=1);
namespace NotificationHub;

/**
 * SubscribeDTO
 * 
 * 
 * @package NotificationHub
 * @subpackage Resource
 * @author sevDesk
 */
final class SubscribeDTO {

    /**
     * @var int
     */
    public $sevUserID;

    /**
     * @var string
     */
    public $uuid;

    /**
     * @var DeviceType
     */
    public $deviceType;

    /**
     * @var String
     */
    public $activeStartTime;

    /**
     * @var String
     */
    public $activeEndTime;

    /**
     * SubscribeDTO constructor.
     * @param int $sevUserID Identifier of owner of device
     * @param string $uuid Identifier of device
     * @param int $deviceType tells us what kind of device we are saving
     */
    public function __construct(int $sevUserID, string $uuid, int $deviceType, String $activeStartTime = null, String $activeEndTime = null){
        $this->sevUserID = $sevUserID;
        $this->uuid = $uuid;
        $this->deviceType = $deviceType;
        $this->activeStartTime = $activeStartTime;
        $this->activeEndTime = $activeEndTime;
    }
}