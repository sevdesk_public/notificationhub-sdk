<?php
declare(strict_types=1);
namespace NotificationHub;
use MyCLabs\Enum\Enum;

/**
 * Event
 * 
 * 
 * @package NotificationHub
 * @subpackage Resource
 * @author sevDesk
 */
interface Event {
    public function getID();
    public function getKey();
    public function getObjectID();
    public function getTitleArgs();
    public function getPayload();
}