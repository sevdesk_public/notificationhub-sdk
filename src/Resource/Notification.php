<?php
declare(strict_types=1);
namespace NotificationHub;

/**
 * Notification
 * 
 * 
 * @package NotificationHub
 * @subpackage Resource
 * @author sevDesk
 */
class Notification
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $deviceUUID;

    /**
     * @var int
     */
    public $eventID;

    /**
     * @var DeviceType
     */
    public $consumerType;

    /**
     * @var bool
     */
    public $isRead;

     /**
     * @var bool
     */
    public $isSend;

    /**
     * @var string
     */
    public $earliestSendTime;

    /**
     * @var string
     */
    public $latestSendTime;

    function __construct($data) 
    {
        foreach($data as $key => $val)
        {
            if(property_exists(__CLASS__,$key))
            {
                if ($key == "consumerType") {
                    $this->$key =  new DeviceType($val);
                } else {
                    $this->$key =  $val;
                }
            }
        }
    }
}