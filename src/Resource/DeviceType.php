<?php
declare(strict_types=1);
namespace NotificationHub;
use MyCLabs\Enum\Enum;

/**
 * DeviceType
 * 
 * 
 * @package NotificationHub
 * @subpackage Resource
 * @author sevDesk
 * @method static \NotificationHub\DeviceType iOS()
 * @method static \NotificationHub\DeviceType ANDROID()
 * @method static \NotificationHub\DeviceType WEB()
 */
class DeviceType extends Enum
{
    private const iOS = 0;
    private const ANDROID = 1;
    private const WEB = 2;
}