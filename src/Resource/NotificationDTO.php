<?php
declare(strict_types=1);
namespace NotificationHub;

/**
 * NotificationDTO
 * 
 * 
 * @package NotificationHub
 * @subpackage Resource
 * @author sevDesk
 */
final class NotificationDTO {

    /**
     * @var string
     */
    public $titleKey;

    /**
     * @var string
     */
    public $titleKeyArgs;

    /**
     * @var string
     */
    public $bodyKey;

    /**
     * @var string[]
     */
    public $bodyKeyArgs;

    /**
     * @var int
     */
    public $objectID;

    /**
     * App_External_NotificationHub_Model_NotificationDTO constructor.
     * @param string $titleKey will be used as translation key for the notification title
     * @param string[] $titleKeyArgs will be used to replace some placeholders in the title (optional)
     * @param string $bodyKey will be used as translation key for the notification body
     * @param string[] $bodyKeyArgs will be used to replace some placeholders in the body (optional)
     * @param int $objectID will be used to replace some placeholders in the body (optional)
     */
    public function __construct($titleKey, $bodyKey, $titleKeyArgs = null, $bodyKeyArgs = null, $objectID = null){
        $this->titleKey = $titleKey;
        $this->titleKeyArgs = $titleKeyArgs;
        $this->bodyKey = $bodyKey;
        $this->bodyKeyArgs = $bodyKeyArgs;
        $this->objectID = $objectID;
    }
}