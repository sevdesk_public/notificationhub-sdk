<?php
declare(strict_types=1);
namespace NotificationHub;
use MyCLabs\Enum\Enum;

/**
 * Endpoint
 * 
 * 
 * @package NotificationHub
 * @subpackage Resource
 * @author sevDesk
 * @method static \NotificationHub\Endpoint DEVELOPMENT()
 * @method static \NotificationHub\Endpoint PRODUCTION()
 */
class Endpoint extends Enum
{
    private const DEVELOPMENT = "https://dev-notificationhub.k8s.sevdesk.dev/v1/notificationhub/";
    private const PRODUCTION = "https://notificationhub.services.sevdesk.dev/v1/notificationhub/";
}