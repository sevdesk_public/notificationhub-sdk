<?php
declare(strict_types=1);
namespace NotificationHub;

/**
 * Device
 * 
 * 
 * @package NotificationHub
 * @subpackage Resource
 * @author sevDesk
 */
class Device
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $sevUser;

    /**
     * @var DeviceType
     */
    public $consumerType;

    /**
     * @var string
     */
    public $uuid;

    /**
     * @var string
     */
    public $activeStartTime;

    /**
     * @var string
     */
    public $activeEndTime;

    function __construct($data) 
    {
        foreach($data as $key => $val)
        {
            if(property_exists(__CLASS__,$key))
            {
                if ($key == "consumerType") {
                    $this->$key =  new DeviceType($val);
                } else {
                    $this->$key =  $val;
                }
            }
        }
    }
}