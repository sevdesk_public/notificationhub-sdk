<?php
declare(strict_types=1);
namespace NotificationHub;
use MyCLabs\Enum\Enum;

/**
 * EventIdentifier
 *
 *
 * @package NotificationHub
 * @subpackage Resource
 * @author sevDesk
 * @method static \NotificationHub\Event MARKETING()
 * @method static \NotificationHub\Event CHECKACCOUNT_TRANSACTION_SYNC()
 * @method static \NotificationHub\Event TASK_DUE()
 */
class EventIdentifier extends Enum
{
    private const MARKETING = 1;
    private const CHECKACCOUNT_TRANSACTION_SYNC = 2;
    private const TASK_DUE = 3;
}