<?php
declare(strict_types=1);
namespace NotificationHub;

/**
 * NotificationConfig
 * 
 * 
 * @package NotificationHub
 * @subpackage Resource
 * @author sevDesk
 */
class NotificationConfig
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $deviceID;

    /**
     * @var int
     */
    public $eventID;

    /**
     * @var bool
     */
    public $isEnabled;

    function __construct($data) 
    {
        foreach($data as $key => $val)
        {
            if(property_exists(__CLASS__,$key))
            {
                $this->$key =  $val;
            }
        }
    }
}