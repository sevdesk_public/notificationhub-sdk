<?php
declare(strict_types=1);
namespace NotificationHub;

/**
 * MarketingEvent
 *
 *
 * @package NotificationHub
 * @subpackage Resource
 * @author sevDesk
 */
class MarketingEvent implements Event
{
    /**
     * @var EventIdentifier
     */
    private $identifier;

    /**
     * @var string
     */
    public $text;

    function __construct($text)
    {
        $this->identifier = EventIdentifier::MARKETING();
        $this->text = $text;
    }

    /**
     * Get event id
     * @return int
     */
    public function getID()
    {
        return $this->identifier->getValue();
    }

    /**
     * Get event name
     * @return string
     */
    public function getKey()
    {
        return $this->identifier->getKey();
    }

    /**
     * Get event id
     * @return int|null
     */
    public function getObjectID()
    {
        return null;
    }

    /**
     * Get arguments for notification title
     * @return string[]
     */
    public function getTitleArgs()
    {
        return [];
    }

    /**
     * Get payload for notification
     * @return string[]
     */
    public function getPayload()
    {
        return [$this->text];
    }
}