<?php
declare(strict_types=1);
namespace NotificationHub;

/**
 * TaskDueEvent
 *
 *
 * @package NotificationHub
 * @subpackage Resource
 * @author sevDesk
 */
class TaskDueEvent implements Event
{
    /**
     * @var EventIdentifier
     */
    private $identifier;

    /**
     * @var int
     */
    public $amount;

    function __construct($amount)
    {
        $this->identifier = EventIdentifier::TASK_DUE();
        $this->amount = $amount;
    }

    /**
     * Get event id
     * @return int
     */
    public function getID()
    {
        return $this->identifier->getValue();
    }

    /**
     * Get event name
     * @return string
     */
    public function getKey()
    {
        return $this->identifier->getKey();
    }

    /**
     * Get event id
     * @return int|null
     */
    public function getObjectID()
    {
        return $this->identifier->getValue();
    }

    /**
     * Get arguments for notification title
     * @return string[]
     */
    public function getTitleArgs()
    {
        return [];
    }

    /**
     * Get payload for notification
     * @return string[]
     */
    public function getPayload()
    {
        return [strval($this->amount)];
    }
}