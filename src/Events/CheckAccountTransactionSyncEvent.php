<?php
declare(strict_types=1);
namespace NotificationHub;

/**
 * MarketingEvent
 *
 *
 * @package NotificationHub
 * @subpackage Resource
 * @author sevDesk
 */
class CheckAccountTransactionSyncEvent implements Event
{
    /**
     * @var EventIdentifier
     */
    private $identifier;

    /**
     * @var int
     */
    public $checkAccountID;

    /**
     * @var int
     */
    public $amount;

    function __construct($checkAccountID, $amount)
    {
        $this->identifier = EventIdentifier::CHECKACCOUNT_TRANSACTION_SYNC();
        $this->checkAccountID = $checkAccountID;
        $this->amount = $amount;
    }

    /**
     * Get event id
     * @return int
     */
    public function getID()
    {
        return $this->identifier->getValue();
    }

    /**
     * Get event name
     * @return string
     */
    public function getKey()
    {
        return $this->identifier->getKey();
    }

    /**
     * Get event id
     * @return int|null
     */
    public function getObjectID()
    {
        return $this->identifier->getValue();
    }

    /**
     * Get arguments for notification title
     * @return string[]
     */
    public function getTitleArgs()
    {
        return [];
    }

    /**
     * Get payload for notification
     * @return string[]
     */
    public function getPayload()
    {
        return [strval($this->amount)];
    }
}