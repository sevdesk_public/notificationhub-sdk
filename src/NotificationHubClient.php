<?php
declare(strict_types=1);
namespace NotificationHub;

use NotificationHub\HubApi;

/**
 * NotificationHubClient
 * 
 * @package NotificationHub
 * @author sevDesk
 */
class NotificationHubClient
{
    /** @var NotificationHubApiInterface  */
    protected $api;
    protected $stringKeyService;

    public function __construct(NotificationHubApiInterface $api)
    {
        $this->api = $api;
        $this->stringKeyService = new StringKeyService();
    }

    /**
     * Get all devices by sevUser id
     * @param int $sevUserID Identifier of current sev user
     * @return Device[]
     */
    public function getDevices(int $sevUserID)
    {
        return $this->api->getDevices($sevUserID);
    }

    /**
     * Get all configs for device
     * @param int $deviceID Identifier of current device
     * @return NotificationConfig[]
     */
    public function getNotificationConfigs(int $deviceID)
    {
        return $this->api->getNotificationConfigs($deviceID);
    }

    /**
     * Get all configs for device
     * @param int $configID identifier of config
     * @param bool $isEnabled Is config is enabled
     * @return NotificationConfig
     */
    public function updateNotificationConfig(int $configID, bool $isEnabled)
    {
        return $this->api->updateNotificationConfig($configID, $isEnabled);
    }

    /**
     * Register device to notification hub
     * @param int $sevUserID Identifier of current sev user
     * @param string $uuid Identifier of subscribing device
     * @param DeviceType $deviceType Type of device (android, ios or web)
     * @param String $activeStartTime (Optional) Earliest time when device can receive notifications
     * @param String $activeEndTime (Optional) Latest time when device can receive notifications
     * @return Device
     */
    public function subscribe(int $sevUserID, string $uuid, DeviceType $deviceType, String $activeStartTime = null, String $activeEndTime = null)
    {
        $dto = new SubscribeDTO($sevUserID, $uuid, $deviceType->getValue(), $activeStartTime, $activeEndTime);
        return $this->api->subscribe($dto);
    }

    /**
     * Remove device from notification hub
     * @param int $deviceID Identifier of device
     * @return Device
     */
    public function unsubscribe(int $deviceID)
    {
        return $this->api->unsubscribe($deviceID);
    }

    /**
     * Push notifications to one or more devices
     * @param int $sevUserID Identifier of current sev user
     * @param Event $event Type of notification
     * @param DeviceType $deviceType (Optional) Type of device (android, ios or web)
     * @param EventPriority $priority (Optional) Priority how important the notification is
     * @return Notification[]
     */
    public function push(int $sevUserID, Event $event, DeviceType $deviceType = null, EventPriority $priority = null)
    {
        $notificationDto = new NotificationDTO($this->stringKeyService->getTitle($event), $this->stringKeyService->getBody($event), $event->getTitleArgs(), $event->getPayload(), $event->getObjectID());
        $eventPriority = $priority ?? EventPriority::NORMAL();
        $consumer = $deviceType ? $deviceType->getValue() : null;
        $pushDto = new PushDTO($sevUserID, $event->getID(), $notificationDto, $consumer, $eventPriority->getValue());
        return $this->api->push($pushDto);
    }
}