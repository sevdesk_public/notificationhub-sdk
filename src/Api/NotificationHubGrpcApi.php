<?php
declare(strict_types=1);
namespace NotificationHub;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

use Api\NotificationData;
use Api\ListDevicesRequest;
use Api\SubscribeRequest;
use Api\UnsubscribeRequest;
use Api\PushRequest;
use Api\NotificationHubClient;

/**
 * NotificationHubGrpcApi
 * 
 * 
 * @package NotificationHub
 * @subpackage Resource
 * @author sevDesk
 */
final class NotificationHubGrpcApi implements NotificationHubApiInterface
{
    /** @var NotificationHubClient  */
    protected $client;

    /**
     * App_External_NotificationHub_API constructor.
     * @param bool $isProduction This parameter defines the endpoint for the notifiction hub
     */
    public function __construct(string $endpoint, string $port)
    {
        $this->client = new NotificationHubClient($endpoint, [
            'credentials' => \Grpc\ChannelCredentials::createInsecure(),
        ]);
    }

    /**
     * Get all devices by sevUser id
     * @param int $sevUserID Identifier of current sev user
     * @return array mixed
     */
    public function getDevices(int $sevUserID) 
    {
        $request = new ListDevicesRequest();
        $request->setSevUser($sevUserID);
        list($response, $status) = $this->client->ListDevices($request)->wait();
        echo json_encode($status);
        if ($status->code !== \Grpc\STATUS_OK) {
            throw new HubException(ErrorCode::GET_DEVICES_FAILED()->getKey(), 500, ErrorCode::GET_DEVICES_FAILED()->getValue());
        } else {
            return $response->getDevices();
        }
    }

    /**
     * Get all configs for given device
     * @param int $deviceID Identifier of current device
     * @return array mixed
     */
    public function getNotificationConfigs(int $deviceID) {
        
    }

    /**
     * Register device to notification hub
     * @param Subscribe $dto This parameter will contain all data for subscribing a device to the notifiction hub
     * @return mixed
     */
    public function subscribe(SubscribeDTO $dto) 
    {
        $request = new SubscribeRequest();
        $request->setUuid($dto->uuid);
        $request->setSevUser($dto->sevUserID);
        $request->setConsumerType($dto->deviceType);
        list($response, $status) = $this->client->Subscribe($request)->wait();
        if ($status->code !== \Grpc\STATUS_OK) {
            throw new HubException(ErrorCode::SUBSCRIBE_FAILED()->getKey(), 500, ErrorCode::SUBSCRIBE_FAILED()->getValue());
        } else {
            return $response->getDevice();
        }
    }

    /**
     * Remove device from notification hub
     * @param int $deviceID id of user device
     * @return mixed
     */
    public function unsubscribe(int $deviceID) 
    {
        $request = new UnsubscribeRequest();
        $request->setDeviceID($deviceID);
        list($response, $status) = $this->client->Unsubscribe($request)->wait();
        if ($status->code !== \Grpc\STATUS_OK) {
            throw new HubException(ErrorCode::UNSUBSCRIBE_FAILED()->getKey(), 500, ErrorCode::UNSUBSCRIBE_FAILED()->getValue());
        } else {
            return $response->getDevice();
        }
    }

    /**
     * Push notifications to one or more devices
     * @param PushDTO $dto This parameter will contain all data for pushing a notification
     * @return mixed
     */
    public function push(PushDTO $dto) 
    {
        $notificationData = new NotificationData();
        $notificationData->setTitleKey($dto->data->titleKey);
        $notificationData->setTitleKeyArgs($dto->data->titleKeyArgs);
        $notificationData->setBodyKey($dto->data->bodyKey);
        $notificationData->setBodyKeyArgs($dto->data->bodyKeyArgs);
        $reust = new PushNotificationRequest();
        $request->setSevUserID($dto->sevUserID);
        $request->setEventID($dto->eventID);
        $request->setData($notificationData);
        $request->setConsumerType($dto->deviceType);
        list($response, $status) = $this->client->Push($request)->wait();
        if ($status->code !== \Grpc\STATUS_OK) {
            throw new HubException(ErrorCode::PUSH_FAILED()->getKey(), 500, ErrorCode::PUSH_FAILED()->getValue());
        } else {
            return $response->getSuccess();
        }
    }
}