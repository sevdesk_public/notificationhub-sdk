<?php
declare(strict_types=1);
namespace NotificationHub;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

/**
 * NotificationHubApi
 * 
 * 
 * @package NotificationHub
 * @subpackage Resource
 * @author sevDesk
 */
final class NotificationHubRestApi implements NotificationHubApiInterface
{
    /** @var \GuzzleHttp\Client  */
    protected $client;

    /**
     * App_External_NotificationHub_API constructor.
     * @param string $endpoint URL
     */
    public function __construct(string $endpoint)
    {
        $this->client = new \GuzzleHttp\Client([
            "base_uri" => $endpoint,
            "headers" => ["Content-Type" => "application/json"]
        ]);
    }

    /**
     * Get all devices by sevUser id
     * @param int $sevUserID Identifier of current sev user
     * @return Device[]
     * @throws HubException
     */
    public function getDevices(int $sevUserID) 
    {
        try {
            $response = $this->client->request("GET", "devices", [
                "query" => ["sevUser" => $sevUserID]
            ]);
            $result = $this->decode($response);
            $mapper = function($value) { return new Device($value); };
            return array_map($mapper, $result);
        } catch (TransferException $exception) {
            $this->handleException($exception);
        }
    }

    /**
     * Get all configs for given device
     * @param int $deviceID Identifier of current device
     * @return NotificationConfig[]
     * @throws HubException
     */
    public function getNotificationConfigs(int $deviceID) {
        try {
            $response = $this->client->request("GET", "notificationConfigs", [
                "query" => ["deviceID" => $deviceID]
            ]);
            $result = $this->decode($response);
            $mapper = function($value) { return new NotificationConfig($value); };
            return array_map($mapper, $result);
        } catch (TransferException $exception) {
            $this->handleException($exception);
        }
    }

    /**
     * Enabled or disabled config for notifications
     * @param int $configID identifier of config
     * @param bool $isEnabled Is config is enabled
     * @return NotificationConfig
     * @throws HubException
     */
    public function updateNotificationConfig(int $configID, bool $isEnabled) {
        try {
            $response = $this->client->request("PUT", "notificationConfigs/" . configID, [
                "json" => ["isEnabled" => $isEnabled]
            ]);
            $result = $this->decode($response);
            $mapper = function($value) { return new NotificationConfig($value); };
            return array_map($mapper, $result);
        } catch (TransferException $exception) {
            $this->handleException($exception);
        }
    }

    /**
     * Register device to notification hub
     * @param SubscribeDTO $dto This parameter will contain all data for subscribing a device to the notifiction hub
     * @return Device
     * @throws HubException
     */
    public function subscribe(SubscribeDTO $dto) 
    {
        try {
            $response = $this->client->request("POST", "devices", [
                "json" => ["sevUser" => $dto->sevUserID, "uuid" => $dto->uuid, "consumerType" => $dto->deviceType, "activeStartTime" => $dto->activeStartTime, "activeEndTime" => $dto->activeEndTime]
            ]);
            $result = $this->decode($response);
            return new Device($result);
        } catch (TransferException $exception) {
            $this->handleException($exception);
        }
    }

    /**
     * Remove device from notification hub
     * @param int $deviceID id of user device
     * @return Device
     * @throws HubException
     */
    public function unsubscribe(int $deviceID) 
    {
        try {
            $response = $this->client->delete("devices/" . strval($deviceID));
            $result = $this->decode($response);
            return new Device($result);
        } catch (TransferException $exception) {
            $this->handleException($exception);
        }
    }

    /**
     * Push notifications to one or more devices
     * @param PushDTO $dto This parameter will contain all data for pushing a notification
     * @return Notification[]
     * @throws HubException
     */
    public function push(PushDTO $dto) 
    {
        try {
            $response = $this->client->request("POST", "notifications", [
                "json" => [
                    "sevUserID" => $dto->sevUserID, 
                    "eventID" => $dto->eventID,
                    "data" => [
                        "titleKey" => $dto->data->titleKey, 
                        "titleKeyArgs" => $dto->data->titleKeyArgs, 
                        "bodyKey" => $dto->data->bodyKey, 
                        "bodyKeyArgs" => $dto->data->bodyKeyArgs,
                        "objectID" => $dto->data->objectID
                    ], 
                    "consumerType" => $dto->deviceType,
                    "eventPriority" => $dto->eventPriority
                ]
            ]);
            $result = $this->decode($response);
            $mapper = function($value) { return new Notification($value); };
            return array_map($mapper, $result);
        } catch (TransferException $exception) {
            $this->handleException($exception);
        }
    }

    /**
     * Decode GuzzleHttp Response to json object
     * @param Psr7\Response $response
     * @return mixed
     */
    private function decode(\GuzzleHttp\Psr7\Response $response) {
        return json_decode($response->getBody()->getContents());
    }

    /**
     * Handle exceptions
     * @param TransferException $exception
     * @throws HubException
     */
    private function handleException(TransferException $exception) {
        if ($exception instanceof \GuzzleHttp\Exception\ClientException) {
            throw new HubException("Request failed with 400 Level error code", ErrorCode::BAD_REQUEST()->getValue());
        } else if ($exception instanceof \GuzzleHttp\Exception\ServerException) {
            $content = json_decode(strval($exception->getResponse()->getBody()), true);
            throw new HubException($content["message"], ErrorCode::SERVER_ERROR()->getValue());
        } else if ($exception instanceof \GuzzleHttp\Exception\ConnectException) {
            throw new HubException("Request failed due network errors", ErrorCode::CONNECTION_ERROR()->getValue());
        } else {
            throw new HubException("Request failed for unknown reason", ErrorCode::UNKNOWN_ERROR()->getValue());
        }
    }
}
