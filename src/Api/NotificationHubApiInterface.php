<?php
declare(strict_types=1);
namespace NotificationHub;

/**
 * NotificationHubApiInterface
 * 
 * 
 * @package NotificationHub
 * @subpackage Resource
 * @author sevDesk
 */
interface NotificationHubApiInterface {
    /**
     * @param int $sevUserID Identifier of current sev user
     * @return Device[]
     */
    public function getDevices(int $sevUserID);

    /**
     * @param int $deviceID Identifier of device
     * @return NotificationConfig[]
     */
    public function getNotificationConfigs(int $deviceID);

    /**
     * @param int $configID identifier of config
     * @param int $configID identifier of config
     * @return NotificationConfig
     */
    public function updateNotificationConfig(int $configID, bool $isEnabled);

    /**
     * @param SubscribeDTO $dto This parameter will contain all data for subscribing a device to the notifiction hub
     * @return Device
     */
    public function subscribe(SubscribeDTO $dto);

    /**
     * @param int $deviceID Identifier of device
     * @return Device
     */
    public function unsubscribe(int $deviceID);

    /**
     * @param PushDTO $dto This parameter will contain all data for pushing a notification
     * @return Notification[]
     */
    public function push(PushDTO $dto);
}