<?php
declare(strict_types=1);
namespace NotificationHub;

/**
 * StringKeyService
 * 
 * 
 * @package NotificationHub
 * @subpackage Resource
 * @author sevDesk
 */
class StringKeyService
{
    public function __construct() {
        $content = file_get_contents(__DIR__ . '/../Assets/string.json');
        $this->strings = json_decode($content, true);
    }

    public function getTitle(Event $event) {
        return $this->getString($event->getKey() . '_TITLE');
    }

    public function getBody(Event $event) {
        return $this->getString($event->getKey() . '_BODY');
    }

    protected function getString($key) {
        return $this->strings[$key];
    }
}