<?php
namespace NotificationHub;

/**
 * HubException
 * 
 * 
 * @package NotificationHub
 * @subpackage Error
 * @author sevDesk
 */
class HubException extends \Exception {
    /**
     * Http status code
     * @var int
     */
    protected $httpCode;

    /**
     * Internal error code
     * @var null
     */
    protected $errorCode;

    /**
     * Inernal error message
     * @var null
     */
    protected $message;

    /**
     * @param string $message
     * @param int $httpCode
     * @param null $sevErrorData
     */
    public function __construct($message = "", $httpCode = 500, $errorCode = null)
    {
        $this->httpCode = $httpCode;
        $this->errorCode = $errorCode;
        $this->message = $message;
    }

    public function getHttpCode() {
        return $this->httpCode;
    }

    public function getErrorCode() {
        return $this->errorCode;
    }
}