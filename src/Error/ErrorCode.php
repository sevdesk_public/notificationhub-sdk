<?php
declare(strict_types=1);
namespace NotificationHub;
use MyCLabs\Enum\Enum;

/**
 * ErrorCode
 * 
 * 
 * @package NotificationHub
 * @subpackage Error
 * @author sevDesk
 */
class ErrorCode extends Enum
{
    private const BAD_REQUEST = 1;
    private const SERVER_ERROR = 2;
    private const CONNECTION_ERROR = 3;
    private const UNKNOWN_ERROR = 4;
    private const NO_STRINGS = 5;
    private const GET_DEVICES_FAILED = 6;
    private const SUBSCRIBE_FAILED = 7;
    private const UNSUBSCRIBE_FAILED = 8;
    private const PUSH_FAILED = 9;
}